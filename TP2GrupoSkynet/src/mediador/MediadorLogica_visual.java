package mediador;

import logica.AGM;
import logica.BFS;
import logica.Estadisticas;
import logica.Grafo;

public class MediadorLogica_visual {

	private Grafo grafo;
	private Grafo arbolMinimo;
	
	private Grafo grafoMinimoEnKPartes;
	
	public MediadorLogica_visual() {
		this.grafo = new Grafo(0);
	}

	/////////////////// Metodos grafo///////////////////////////
	public void agregarNVertices(int cantVertices) {
		reiniciarGrafos();
		grafo.agregarNVertices(cantVertices);

	}

	public void agregarArista(int puntoA, int puntoB, int pesoArista) {

		this.grafo.agregarArista(puntoA, puntoB, pesoArista);

	}
	protected int getCantidadVertices() {
		return this.grafo.getCantidadDeVertices();
		
	}
	
	protected boolean existeArista(int puntoA, int puntoB) {
		
		return this.grafo.existeAristaEntre(puntoA, puntoB);
	}
	
	

	///////////////// METODOS AGM//////////////////////////////

	public boolean crearAGM() {

		if (BFS.esConexo(grafo)) {
			
			this.arbolMinimo = AGM.generarAGM(grafo);
		
			return true;
		}

		return false;

	}
	

	public int getCantAristasAGM() {

		return this.arbolMinimo.cantAristas();
	}

	public int getCantAristasAGMmenosk() {

		return this.grafoMinimoEnKPartes.cantAristas();
	}

	public boolean grafoSinAristas() {

		return this.grafo.cantAristas() == 0;

	}
	
	private int getCantidadVerticesAgm() {
		
		return this.arbolMinimo.getCantidadDeVertices();
	}
	

	public int getIDExtremo1Arista(int i) {

		return this.arbolMinimo.getIDExtremo1Arista(i);

	}

	public int getIDExtremo2Arista(int i) {

		return this.arbolMinimo.getIDExtremo2Arista(i);
	}

	////////////////////// GRAFO MINIMO EN K PARTES///////////
	public boolean restarKmenos1Aristas(int k) {
		crearAGM();
		if (arbolMinimo != null&&k<=this.getCantidadVerticesAgm()) {
			this.grafoMinimoEnKPartes = this.arbolMinimo.generarRegiones(k);
			
			return true;
		}

		return false;
	}
	
//	public boolean kMenos1esMenorALaCantVertices (int i) {
//		
//		return i>this.getCantAristasAGMmenosk();
//		
//	}
	
	
	
	

	////////////////////// Estadistica////////////////////////

	public String estadisticaGrafo() {

		return Estadisticas.mostrarEstadisticas(this.grafo);
	}

	public String estadisticaArbolMinimo() {

		return Estadisticas.mostrarEstadisticas(this.arbolMinimo);
	}

	public String estadisticaArbolMinimoEnKPartes() {

		return Estadisticas.mostrarEstadisticas(this.grafoMinimoEnKPartes);
	}

	////////////////////// reiniciarGRafos///////////////

	public void reiniciarGrafos() {
		this.arbolMinimo = null;
		this.grafoMinimoEnKPartes = null;
		this.grafo = new Grafo(0);

	}

}
