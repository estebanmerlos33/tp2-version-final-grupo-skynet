package sonido;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Musica {
	private Clip clip;

	public Musica() {
		try {
			AudioInputStream input = AudioSystem.getAudioInputStream(new File("los-palmeras-bombon-asesino.wav").getAbsoluteFile());
			clip = AudioSystem.getClip();
			clip.open(input);

		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
			System.out.println(ex.getCause());
		}

	}

	public void start() {// Inicia el clip de musica
		this.clip.setFramePosition(0);
		this.clip.start();
	}

	public void stop() {// Detiene el clip de musica
		this.clip.stop();

	}
}
