package logica;

import static org.junit.Assert.*;

import org.junit.Test;

public class _AristaTest {

	@Test
	public void testAristaValida() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Arista a = new Arista(v, w, 20);
		boolean resultado = a.getExtremo1().compareTo(v) == 0 && a.getExtremo2().compareTo(w) == 0 && a.getPeso() == 20;
		assertTrue(resultado);
	}

	@Test(expected = NullPointerException.class)
	public void testAristaInvalidaPorVerticeNull() {
		Vertice v = new Vertice(0);
		Vertice w = null;
		@SuppressWarnings("unused")
		Arista a = new Arista(v, w, 20);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAristaInvalidaPorMismoVerticeEnExtremos() {
		Vertice v = new Vertice(0);
		@SuppressWarnings("unused")
		Arista a = new Arista(v, v, 20);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAristaInvalidaPorPeso() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		@SuppressWarnings("unused")
		Arista a = new Arista(v, w, -1);

	}

	@Test
	public void testSonIguales() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Arista a = new Arista(v, w, 20);

		Arista b = new Arista(v, w, 20);

		assertEquals(0, a.compareTo(b));
	}

	@Test
	public void testSonDistintasPorUnVertice() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Vertice z = new Vertice(2);

		Arista a = new Arista(v, w, 20);
		Arista b = new Arista(v, z, 20);

		assertNotEquals(0, a.compareTo(b));
	}

	@Test
	public void testSonDistintasPorPeso() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);

		Arista a = new Arista(v, w, 10);
		Arista b = new Arista(v, w, 20);

		assertNotEquals(0, a.compareTo(b));
	}

}
