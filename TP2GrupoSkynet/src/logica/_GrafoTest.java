package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

public class _GrafoTest {

	@Test
	public void testGrafoValido() {
		Grafo g = new Grafo(3);
		boolean gTiene3Vertices = g.getCantidadDeVertices() == 3;
		boolean gNoTieneAristas = g.getAristas().size() == 0;
		assertTrue(gTiene3Vertices && gNoTieneAristas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGrafoInvalido() {
		@SuppressWarnings("unused")
		Grafo g = new Grafo(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAristaInvalidoPorVerticeNegativo() {
		Grafo g = new Grafo(3);
		g.agregarArista(-1, 0, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAristaInvalidoPorVerticeInexistente() {
		Grafo g = new Grafo(3);
		g.agregarArista(1, 3, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAgregarAristaInvalidoPorPesoNegativo() {
		Grafo g = new Grafo(3);
		g.agregarArista(1, 2, -1);
	}

	@Test
	public void existeAristaEntre() {
		Grafo g = new Grafo(3);
		g.agregarArista(1, 2, 10);
		assertTrue(g.existeAristaEntre(1, 2));
	}

//	@Test(expected = IllegalArgumentException.class)
//
//	public void testAgregarAristaInvalidaYaExistente() {
//		Grafo g = new Grafo(3);
//		g.agregarArista(1, 2, 10);
//		g.agregarArista(1, 2, 10);
//
//	}

	public void testAgregarAristaValida() {
		Grafo g = new Grafo(3);
		g.agregarArista(0, 1, 10);
		boolean vertice0esVecinoDe1 = g.getVertices().get(0).esVecinoDe(g.getVertices().get(1));
		boolean vertice1esVecinoDe0 = g.getVertices().get(1).esVecinoDe(g.getVertices().get(0));

		boolean laAristaTieneExtremo0 = g.getAristas().get(0).getExtremo1().compareTo(g.getVertices().get(0)) == 0;
		boolean laAristaTieneExtremo1 = g.getAristas().get(1).getExtremo1().compareTo(g.getVertices().get(1)) == 0;

		boolean laAristaTienePeso10 = g.getAristas().get(0).getPeso() == 10;

		assertTrue(vertice0esVecinoDe1 && vertice1esVecinoDe0 && laAristaTieneExtremo0 && laAristaTieneExtremo1
				&& laAristaTienePeso10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testQuitarAristaInvalidaVerticeNegativo() {
		Grafo g = new Grafo(3);
		g.quitarArista(-1, 0);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testQuitarAristaInvalidaVerticeInexistente() {
		Grafo g = new Grafo(3);
		g.quitarArista(0, 4);

	}

	@Test
	public void testQuitarAristaValida() {
		Grafo g = new Grafo(3);
		g.agregarArista(0, 1, 10);
		g.quitarArista(0, 1);
		boolean vertice0NoEsVecinoDe1 = !g.getVertices().get(0).esVecinoDe(g.getVertices().get(1));
		boolean vertice1NoEsVecinoDe0 = !g.getVertices().get(1).esVecinoDe(g.getVertices().get(0));
		boolean gNoTieneAristas = g.getAristas().size() == 0;
		assertTrue(vertice0NoEsVecinoDe1 && vertice1NoEsVecinoDe0 && gNoTieneAristas);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testQuitarAristaGrafoSinAristas() {
		Grafo g = new Grafo(3);
		g.quitarArista(1, 2);

	}

	@Test(expected = RuntimeException.class)
	public void testQuitarAristaDeMayorPesoGrafoSinAristas() {
		Grafo g = new Grafo(3);
		g.quitarAristaDeMayorPeso();

	}

	@Test
	public void testQuitarAristaDeMayorPeso() {
		Grafo g = new Grafo(5);

		g.agregarArista(0, 1, 10);
		g.agregarArista(1, 2, 30);
		g.agregarArista(2, 3, 5);
		g.agregarArista(2, 4, 50);// Debe eliminar esta arista
		g.agregarArista(3, 4, 20);

		g.quitarAristaDeMayorPeso();

		boolean hay4Aristas = g.getAristas().size() == 4;
		boolean vertice2NoEsVecinoDe4 = !g.getVertices().get(2).esVecinoDe(g.getVertices().get(4));
		boolean vertice4NoEsVecinoDe2 = !g.getVertices().get(4).esVecinoDe(g.getVertices().get(2));

		assertTrue(hay4Aristas && vertice2NoEsVecinoDe4 && vertice4NoEsVecinoDe2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void generarRegionesMayoresAVertices() {
		Grafo g = new Grafo(1);
		g.generarRegiones(2);
	}

	@Test
	public void generar3RegionesGrafoCamino() {
		Grafo g = new Grafo(6);

		g.agregarArista(0, 1, 10);
		g.agregarArista(1, 2, 50);
		g.agregarArista(2, 3, 30);
		g.agregarArista(3, 4, 50);
		g.agregarArista(4, 5, 50);

		AGM.generarAGM(g);
		g.generarRegiones(3);

		int[] raicesEsperadas = { 1, 3, 5 };

		assertTrue(sonIguales(raicesEsperadas, (ArrayList<Integer>) g.getRaices()));

	}

	private static boolean sonIguales(int[] esperado, ArrayList<Integer> resultado) {
		if (esperado.length != resultado.size())
			return false;

		for (Integer res : resultado) {
			if (!resultado.contains(res))
				return false;
		}

		return true;
	}
}
