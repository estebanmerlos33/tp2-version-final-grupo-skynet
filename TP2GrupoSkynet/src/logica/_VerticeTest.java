package logica;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class _VerticeTest {

	@Test
	public void testVerticeValido() {
		boolean resultado = false;
		Vertice v = new Vertice(0);
		if (v.getId() == 0 && v.getPadre() == v.getId() && v.getVecinos().isEmpty()) {
			resultado = true;
		}
		assertTrue(resultado);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testVerticeInvalidoPorId() {
		@SuppressWarnings("unused")
		Vertice u = new Vertice(-1);
	}

	@Test
	public void testSonIguales() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(0);

		assertEquals(0, v.compareTo(w));
	}

	@Test
	public void testSonDistintos() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		assertEquals(-1, v.compareTo(w));
	}

	@Test(expected = NullPointerException.class)
	public void esVecinoDeNull() {
		Vertice v = new Vertice(0);
		Vertice w = null;
		v.esVecinoDe(w);
	}

	@Test
	public void testEsVecinoDe() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Vertice z = new Vertice(2);

		v.agregarVecino(w);
		w.agregarVecino(v);

		boolean resultado = v.esVecinoDe(w) && w.esVecinoDe(v) && !v.esVecinoDe(z);

		assertTrue(resultado);

	}

	@Test(expected = NullPointerException.class)
	public void testAgregarVecinoNull() {
		Vertice v = new Vertice(0);
		Vertice w = null;
		v.agregarVecino(w);
	}

	@Test
	public void testAgregarVecino() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		v.agregarVecino(w);
		w.agregarVecino(v);
		boolean resultado = v.getVecinos().size() == 1 && w.getVecinos().size() == 1 && v.esVecinoDe(w)
				&& w.esVecinoDe(v);
		assertTrue(resultado);
	}

	@Test(expected = NullPointerException.class)
	public void testQuitarVecinoNull() {
		Vertice v = new Vertice(0);
		Vertice w = null;
		v.quitarVecino(w);
	}

	@Test
	public void testQuitarVecino() {
		Vertice v = new Vertice(0);
		Vertice w = new Vertice(1);
		Vertice z = new Vertice(2);
		v.agregarVecino(w);
		v.agregarVecino(z);
		v.quitarVecino(z);
		boolean resultado = v.getVecinos().size() == 1 && w.getVecinos().size() == 1 && v.esVecinoDe(w)
				&& !v.esVecinoDe(z);
		assertTrue(resultado);
	}

}
