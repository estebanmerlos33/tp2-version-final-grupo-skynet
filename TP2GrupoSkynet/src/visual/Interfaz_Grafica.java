package visual;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import mediador.MediadorLogica_visual;
import sonido.Musica;

import javax.swing.JPanel;
import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import javax.swing.SwingUtilities;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JButton;

import javax.swing.JTextArea;

public class Interfaz_Grafica {

	private JFrame frame;

	private Mapa mapaClase;
	private final JPanel panel = new JPanel();
	private JPanel panelMapa, panelControlMatriz, panelMatrizDeAdya;
	private ButtonGroup grupoJradioButton;
	private JRadioButton AgregarMarcadores, cursor;
	private ArrayList<ArrayList<JCheckBox>> botones;
	private MediadorLogica_visual mediador;
	private JButton btnRemoverObj;
	private JTextArea txtrEstadistica;
	private JScrollPane scrollPane;
	private JButton botonPlay, botonStop, btnIngresarAristas, btnFormarGrafo, btnGenerarAgm, btnSepararEnK;
	private Musica musica;
	private Font font;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz_Grafica window = new Interfaz_Grafica(null, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// contructor del JFRAME
	public Interfaz_Grafica(Mapa mapa, MediadorLogica_visual mediador) {
		initialize(mapa, mediador);
	}

	// inicializa todo los componentes
	private void initialize(Mapa mapa1, MediadorLogica_visual mediador) {

		// INICIALIZA LA VENTANA

		frame = new JFrame();
		frame.setBounds(1600, 800, 1600, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		panel.setBounds(12, 75, 789, 530);
		frame.setResizable(false);
		panel.setLayout(new BorderLayout(0, 0));

		// INICIALIZA EL MAPA
		this.mapaClase = mapa1;
		panel.add(this.mapaClase.getMap());

		// INICIALIZAR EL GRAFO
		this.mediador = mediador;

		// INICIALIZA LA MUSICA :)
		this.musica = new Musica();

		// INICIALIZA EL ArrayList anidado de CHECKBOXES
		this.botones = new ArrayList<ArrayList<JCheckBox>>();

		// AGREGAMOS EL PANEL AL FRAME

		frame.getContentPane().add(panel);

		// INICIALIZAMOS Y AGREGAMOS EN EL FRAME EL PANEL MAPA
		this.panelMapa = new JPanel();
		this.panelMapa.setBounds(12, 0, 789, 33);
		frame.getContentPane().add(panelMapa);
		panelMapa.setLayout(null);

		// INICIALIZAMOS EL PANEL MATRIZ DE ADYACENCIA
		this.panelMatrizDeAdya = new JPanel();

		this.panelMatrizDeAdya.setBounds(825, 70, 752, 781);

		frame.getContentPane().add(panelMatrizDeAdya);

		// Inicializa y configura los Jradio button
		this.grupoJradioButton = new ButtonGroup();

		this.AgregarMarcadores = new JRadioButton("Agregar Marcadores");
		AgregarMarcadores.setBounds(103, 0, 170, 23);
		this.grupoJradioButton.add(AgregarMarcadores);
		panelMapa.add(AgregarMarcadores);

		this.cursor = new JRadioButton("Cursor");
		this.cursor.setSelected(true);
		this.cursor.setBounds(19, 0, 80, 23);
		this.grupoJradioButton.add(cursor);
		panelMapa.add(cursor);

		botonStop = new JButton(":(");
		botonStop.setBounds(717, -1, 44, 25);
		panelMapa.add(botonStop);

		botonPlay = new JButton(":)");
		botonPlay.setBounds(673, -1, 44, 25);
		panelMapa.add(botonPlay);

		botonPlay.addActionListener(e -> {
			this.musica.start();

		});

		botonStop.addActionListener(e -> {
			this.musica.stop();

		});

		// INICIALIZA EL PANEL QUE CONTROLA LA MATRIZ DE ADYACENCIA

		this.panelControlMatriz = new JPanel();
		panelControlMatriz.setBounds(982, 0, 397, 58);
		frame.getContentPane().add(panelControlMatriz);
		panelControlMatriz.setLayout(new BorderLayout(0, 0));

		this.btnFormarGrafo = new JButton("Formar grafo");
		panelControlMatriz.add(btnFormarGrafo, BorderLayout.CENTER);

		this.btnGenerarAgm = new JButton("Generar AGM");
		btnGenerarAgm.setBounds(441, 45, 139, 25);
		btnGenerarAgm.addActionListener(e -> {
			this.crearAGM();
		});

		btnGenerarAgm.setBackground(Color.red);
		this.btnFormarGrafo.setBackground(Color.MAGENTA);

		frame.getContentPane().add(btnGenerarAgm);

		// SE INICILIZAN LOS BOTONES PARA SEPARAR EN K PARTES UN AGM
		// Y EL BOTON PARA LIMPIAR LA PANTALLA
		btnSepararEnK = new JButton("Separar en K Componentes");
		btnSepararEnK.addActionListener(e -> {
			this.separarEnKComp();

		});
		btnSepararEnK.setBounds(201, 45, 228, 25);
		this.btnSepararEnK.setBackground(Color.ORANGE);
		frame.getContentPane().add(btnSepararEnK);

		btnRemoverObj = new JButton("Reiniciar");
		btnRemoverObj.addActionListener(e -> {
			removerObjMapa();

		});

		btnRemoverObj.setBounds(42, 45, 147, 25);
		frame.getContentPane().add(btnRemoverObj);

		// SE SETEA EL JTEXT AREA PARA LAS ESTADISTICAS
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 616, 789, 235);
		frame.getContentPane().add(scrollPane);

		txtrEstadistica = new JTextArea();
		scrollPane.setViewportView(txtrEstadistica);
		txtrEstadistica.setEditable(false);
		txtrEstadistica.setText("Estadisticas");

		this.font = new Font("Tahoma", Font.BOLD, 15);

		txtrEstadistica.setFont(this.font);

		// INICIALIZA LOS BOTONES QUE CONTROLAN LA MATRIZ DE ADYACENCIA
		this.btnIngresarAristas = new JButton("Ingresar Aristas");
		btnIngresarAristas.setBounds(598, 45, 176, 25);
		frame.getContentPane().add(btnIngresarAristas);

		btnIngresarAristas.addActionListener(e -> {
			this.mapaClase.eliminarPoligonos();
			mostrarSeleccionadorAristas();

		});

		btnFormarGrafo.addActionListener(e -> {

			this.asignarArista();

		});

		frame.setVisible(true);

	}

/////////////////////////METODOS MAPA///////////////////////////////////////////////////

	protected void agregarMarcador(Point puntoAlHacerCLick) {

		this.mapaClase.agregarMarcador(this.mapaClase.getCoordinates(puntoAlHacerCLick));
		this.panel.add(this.mapaClase.getMap());
	}

	// Crea una matriz de n * n Jcheckbox,
	// N =cantidad de vertices
	private void mostrarSeleccionadorAristas() {
		this.botones = new ArrayList<ArrayList<JCheckBox>>();

		int tamanoMatrizAd = this.mapaClase.getCantidadCoordenadas();

		if (tamanoMatrizAd == 0) {
			JOptionPane.showMessageDialog(null, "Conjunto de vertices vacio");
			return;
		}

		this.mediador.agregarNVertices(tamanoMatrizAd);

		this.panelMatrizDeAdya.removeAll();

		GridLayout layoutGrid = new GridLayout(tamanoMatrizAd, tamanoMatrizAd); // layout de n*n

		this.panelMatrizDeAdya.setLayout(layoutGrid); // seteamos el layout

		ArrayList<JCheckBox> botones; // arrayList de JCheckBox

		for (int i = 0; i < tamanoMatrizAd; i++) {

			botones = new ArrayList<JCheckBox>();
			for (int j = 0; j < tamanoMatrizAd; j++) {

				JCheckBox boton = new JCheckBox();
				boton.setText(i + ":" + j);

				if (i == j) // si el el checkbox es un bucle, se lo desactiva.
					boton.setEnabled(false);

				if (i < j) // si el el checkbox es un bucle, se lo desactiva.
					boton.setEnabled(false);

				boton.setPreferredSize(new Dimension(15, 15));

				botones.add(boton);
				// [boton1 boton2 boton3],[boton 11 boton 12 ,boton 13]
				this.panelMatrizDeAdya.add(boton);

			}

			this.botones.add(botones);

		}

		this.txtrEstadistica.setText(this.mediador.estadisticaGrafo());

		this.panelMatrizDeAdya.repaint();
		this.panelMatrizDeAdya.revalidate();

		SwingUtilities.updateComponentTreeUI(this.panelMatrizDeAdya);

	}

	// Mira la matriz de JcheckBox se fija cual esta seleccionado y pide el
	// peso(agregar solo numeros)
	private void asignarArista() {

		this.mapaClase.eliminarPoligonos();

		for (int i = 0; i < this.botones.size(); i++) {

			for (int j = 0; j < this.botones.size(); j++) {

				if (this.botones.get(i).get(j).isSelected()) {
					String peso = JOptionPane.showInputDialog("Agregar Peso arista:" + i + "---->" + j);
					int pesoCast = verificacionInput(peso);

					if (!(mayorA0(pesoCast)))
						pesoCast = verificacionInputNegativo(pesoCast);

					this.mapaClase.agregarPoligonoDadosDosMarcadores(i, j, Color.MAGENTA);

					this.mediador.agregarArista(i, j, pesoCast);

				}

			}
		}

		this.txtrEstadistica.setText(this.mediador.estadisticaGrafo());

	}

	// Crea un arbol generador minimo a partir del grafo creado
	private void crearAGM() {
		if (this.mediador.grafoSinAristas()) {
			JOptionPane.showMessageDialog(null, "Es necesario crear aristas");
			return;
		}

		this.mapaClase.eliminarPoligonos();

		if (!(this.mediador.crearAGM())) {
			JOptionPane.showMessageDialog(null, "El grafo armado no es conexo");
			return;
		}

		for (int i = 0; i < this.mediador.getCantAristasAGM(); i++) {

			this.mapaClase.agregarPoligonoDadosDosMarcadores(this.mediador.getIDExtremo1Arista(i),
					this.mediador.getIDExtremo2Arista(i), Color.red);
		}

		this.txtrEstadistica.setText(this.mediador.estadisticaArbolMinimo());

	}

	/// Separa en k partes el arbol generador minimo
	private void separarEnKComp() {
		String kComponentes = JOptionPane.showInputDialog("�En cuantas componentes quiere separar su AGM?");

		int kcomParse = verificacionInputAlQuitarAris(kComponentes);

		if (!(mayorA0(kcomParse))) {
			kcomParse = verificacionInputNegativo(kcomParse);
		}

		this.mapaClase.eliminarPoligonos();

		if (!(this.mediador.restarKmenos1Aristas(kcomParse))) {
			JOptionPane.showMessageDialog(null,
					"Debe crear El arbol generador minimo o el numero ingresado es > a la cantidad de vertices ");

			return;
		}

		for (int i = 0; i < this.mediador.getCantAristasAGMmenosk(); i++) {

			this.mapaClase.agregarPoligonoDadosDosMarcadores(this.mediador.getIDExtremo1Arista(i),
					this.mediador.getIDExtremo2Arista(i), Color.ORANGE);

		}

		this.txtrEstadistica.setText(this.mediador.estadisticaArbolMinimoEnKPartes());

	}

/////////////////LIMPIAR PANTALLA/////////////////////////////////////	

	private void removerObjMapa() {
		this.mapaClase.eliminarElementosMapa();
		this.mediador.reiniciarGrafos();
		this.cursor.setSelected(true);
		this.botones = new ArrayList<ArrayList<JCheckBox>>();
		this.panelMatrizDeAdya.removeAll();
		this.txtrEstadistica.setText("Estadisticas");
		SwingUtilities.updateComponentTreeUI(this.panelMatrizDeAdya);

	}

/////////////////////Metodo JradioButton////////////////////////////////////	

	protected boolean estaApretadoMarcador() {
		return this.AgregarMarcadores.isSelected();
	}

///////////////////Funciones auxiliares//////////////////////////////////////////	

	private int verificacionInput(String str) {

		try {
			return Integer.parseInt(str);
		}

		catch (Exception e) {
			str = JOptionPane.showInputDialog("El peso es un entero positivo");
		}

		return verificacionInput(str);
	}

	private int verificacionInputNegativo(int numero) {

		String str = JOptionPane.showInputDialog("Introducir un numero mayor a 0");
		numero = verificacionInput(str);

		return numero > 0 ? numero : verificacionInputNegativo(numero);

	}

	public boolean mayorA0(int n) {

		return n >= 0;

	}

	private int verificacionInputAlQuitarAris(String str) {
		try {
			return Integer.parseInt(str);
		}

		catch (Exception e) {
			str = JOptionPane.showInputDialog("Se debe ingresar un entero positivo");
		}

		return verificacionInputAlQuitarAris(str);
	}

}